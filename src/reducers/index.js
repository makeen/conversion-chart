import { combineReducers } from 'redux';
import conversionChartReducer from './conversionChartReducer';

const rootReducer = combineReducers({
    conversionChartReducer,
});

export default rootReducer;
