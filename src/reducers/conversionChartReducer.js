import cloneDeep from 'lodash/cloneDeep';
import initialState from './initialState';
import { 
    INITIALIZE_ROWS, 
    UPDATE_INPUT_VALUE, 
    UPDATE_INPUT_UNITS, 
    UPDATE_TARGET_UNITS, 
    CHECK_STUDENT_RESPONSE 
} from '../constants/actionTypes';

const convert = require('convert-units');

const conversionChartReducer = (
  state = initialState.conversionChart,
  action
) => {

    const stateClone = cloneDeep(state);
    let row = null;
    let correctResponse = '';
    switch (action.type) {
        case INITIALIZE_ROWS:
            const updatedRows = [];
            for(let i = 0; i < action.rows; i++){
                updatedRows.push({
                    inputValue: '0',
                    inputUnits: 'F',
                    targetUnits: 'R',
                    studentResponse: '',
                    outputResult: 'incorrect',
                })
            }
            stateClone.rows = updatedRows;
            return stateClone;
        case UPDATE_INPUT_VALUE:
            stateClone.rows[action.rowIndex].inputValue = action.value;

            // do conversion check here
            row = stateClone.rows[action.rowIndex];
            correctResponse = convert(row.inputValue).from(row.inputUnits).to(row.targetUnits);
            console.log('This is the coorect answer accordng to the library -->', correctResponse);
            if(isNaN(parseFloat(row.inputValue)) || isNaN(parseFloat(action.value))){
                stateClone.rows[action.rowIndex].outputResult = 'invalid';
            }
            else if(correctResponse.toString() === action.value){
                stateClone.rows[action.rowIndex].outputResult = 'correct';
            }
            else{
                stateClone.rows[action.rowIndex].outputResult = 'incorrect';
            }

            return stateClone;
        case UPDATE_INPUT_UNITS:
            if(action.value === stateClone.rows[action.rowIndex].targetUnits){
                alert('Input units can not be the same as the target units');
                return stateClone;
            }
            stateClone.rows[action.rowIndex].inputUnits = action.value;
            return stateClone;
        case UPDATE_TARGET_UNITS:
            if(action.value === stateClone.rows[action.rowIndex].inputUnits){
                alert('Target units can not be the same as the input units');
                return stateClone;
            }
            stateClone.rows[action.rowIndex].targetUnits = action.value;
            return stateClone;
        case CHECK_STUDENT_RESPONSE:
            stateClone.rows[action.rowIndex].studentResponse = action.value;
            
            // do conversion check here too
            row = stateClone.rows[action.rowIndex];
            correctResponse = convert(row.inputValue).from(row.inputUnits).to(row.targetUnits);
            console.log('This is the coorect answer accordng to the library -->', correctResponse);
            if(isNaN(parseFloat(row.inputValue)) || isNaN(parseFloat(action.value))){
                stateClone.rows[action.rowIndex].outputResult = 'invalid';
            }
            else if(correctResponse.toString() === action.value){
                stateClone.rows[action.rowIndex].outputResult = 'correct';
            }
            else{
                stateClone.rows[action.rowIndex].outputResult = 'incorrect';
            }

            return stateClone;                                                  
        default:
            return state;
    }
};

export default conversionChartReducer;