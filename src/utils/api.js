import axios from 'axios';

class Api {
  static headers() {
    return {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    };
  }

  static get(route) {
    return this.xhr(route, null, 'get');
  }

  static put(route, params) {
    return this.xhr(route, params, 'put');
  }

  static post(route, params) {
    return this.xhr(route, params, 'post');
  }

  static delete(route, params) {
    return this.xhr(route, params, 'delete');
  }

  static xhr(url, params, method) {
    return new Promise((resolve) => {
      const options = {
        url,
        data: (params === undefined) ? null : JSON.stringify(params),
        headers: Api.headers(),
        method
      };

      axios(options)
        .then((resp) => {
          resolve(resp);
        })
        .catch((err) => {
          resolve({
            err
          });
        });
    });
  }
}

export default Api;
