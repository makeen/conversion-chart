const conversionChart = (state) => {
    return {
        rows: state.conversionChartReducer.rows,
    }
};
  
export default conversionChart;
  