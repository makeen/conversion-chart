import * as types from '../constants/actionTypes';

export const initializeRows = (rows = 0) => async (dispatch) => {
    dispatch({
        type: types.INITIALIZE_ROWS,
        rows, 
    })
};

export const updateInputValue = (value, rowIndex) => async (dispatch) => {
    dispatch({
        type: types.UPDATE_INPUT_VALUE,
        value,
        rowIndex 
    })
};

export const updateInputUnits = (value, rowIndex) => async (dispatch) => {
    dispatch({
        type: types.UPDATE_INPUT_UNITS,
        value, 
        rowIndex
    })
};

export const updateTargetUnits = (value, rowIndex) => async (dispatch) => {
    dispatch({
        type: types.UPDATE_TARGET_UNITS,
        value,
        rowIndex 
    })
};

export const checkStudentResponse = (value, rowIndex) => async (dispatch) => {
    dispatch({
        type: types.CHECK_STUDENT_RESPONSE,
        value,
        rowIndex 
    })
};