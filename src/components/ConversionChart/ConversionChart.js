import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import * as conversionChartActions from '../../actions/conversionChart';
import conversionChartSelector from '../../selectors/conversionChart';

const classes = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      overflowX: 'auto',
    },
    table: {
      minWidth: 700,
    },
});

const columns = [
    { name: 'temp', numeric: false, disablePadding: false, label: 'Input Temperature' },
    { name: 'input units', numeric: false, disablePadding: false, label: 'Input Units' },
    { name: 'target units', numeric: false, disablePadding: false, label: 'Target Units' },
    { name: 'response', numeric: false, disablePadding: false, label: 'Student Response' },
    { name: 'output', numeric: false, disablePadding: false, label: 'Output' },
];

class ConversionChart extends Component {

    handleInitializeRows(e){
        this.props.actions.initializeRows(e.target.value);
    }

    handleUpdateInputValue(e, index){
        this.props.actions.updateInputValue(e.target.value, index);
    }

    handleChangeInputUnits(e, index){
        this.props.actions.updateInputUnits(e.target.value, index);
    }

    handleChangeTargetUnits(e, index){
        this.props.actions.updateTargetUnits(e.target.value, index);
    }

    handleCheckResponse(e, index){
        this.props.actions.checkStudentResponse(e.target.value, index);
    }

    render(){
        return (
            <Paper className={classes.root}>
                <TextField
                    id="filled-number"
                    label="# of questions to check"
                    value={this.props.rows.length}
                    onChange={(e) => this.handleInitializeRows(e)}
                    type="number"
                    className={classes.textField}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    margin="normal"
                    variant="filled"
                />
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                      {columns.map(column => {
                          return (
                              <TableCell
                                  key={column.name}
                                  numeric={column.numeric}
                                  padding={column.disablePadding ? 'none' : 'default'}
                              >
                                {column.label}
                              </TableCell>
                          )
                      })}
                  </TableRow>
                </TableHead>
                <TableBody>
                    {this.props.rows.map((row, index) => {
                        return(
                            <TableRow key={index}>
                            <TableCell>
                                <TextField
                                    id="input-value"
                                    value={row.inputValue}
                                    onChange={(e) => this.handleUpdateInputValue(e, index)}
                                    className={classes.textField}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    margin="normal"
                                    variant="filled"
                                />
                            </TableCell>
                            <TableCell>
                                <Select
                                    value={row.inputUnits}
                                    onChange={(e) => this.handleChangeInputUnits(e, index)}
                                    inputProps={{
                                    name: 'input-units',
                                    id: 'input-units',
                                    }}
                                >
                                    <MenuItem value={'R'}>Rankine</MenuItem>
                                    <MenuItem value={'K'}>Kelvin</MenuItem>
                                    <MenuItem value={'F'}>Fahrenheit</MenuItem>
                                    <MenuItem value={'C'}>Celsius</MenuItem>
                                </Select>
                            </TableCell>
                            <TableCell>
                                <Select
                                    value={row.targetUnits}
                                    onChange={(e) => this.handleChangeTargetUnits(e, index)}
                                    inputProps={{
                                    name: 'target-units',
                                    id: 'target-units',
                                    }}
                                >
                                    <MenuItem value={'R'}>Rankine</MenuItem>
                                    <MenuItem value={'K'}>Kelvin</MenuItem>
                                    <MenuItem value={'F'}>Fahrenheit</MenuItem>
                                    <MenuItem value={'C'}>Celsius</MenuItem>
                                </Select>
                            </TableCell>
                            <TableCell>
                                <TextField
                                    id="student-response"
                                    value={row.studentResponse}
                                    onChange={(e) => this.handleCheckResponse(e, index)}
                                    className={classes.textField}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    margin="normal"
                                    variant="filled"
                                />
                            </TableCell>
                            <TableCell>
                                <TextField
                                    id="question-result"
                                    value={row.outputResult}
                                    disabled={true}
                                    className={classes.textField}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    margin="normal"
                                    variant="filled"
                                />
                            </TableCell>
                        </TableRow>
                        )
                    })}
                </TableBody>
              </Table>
            </Paper>
          );
    }
}

ConversionChart.propTypes = {
    rows: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => {
    return {
        rows: conversionChartSelector(state).rows
    }
}
  
const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators({
        ...conversionChartActions,
        }, dispatch)
    }
}
  
export default connect(mapStateToProps, mapDispatchToProps)(ConversionChart);
  
