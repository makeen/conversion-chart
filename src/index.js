import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import ConversionChart from './components/ConversionChart';
import registerServiceWorker from './registerServiceWorker';
import {configureStore} from './store/configureStore';

const store = configureStore();

ReactDOM.render(<ConversionChart store={store} />, document.getElementById('root'));
registerServiceWorker();
