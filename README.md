# Conversion Chart Challenge Project

A sample react project. 

### Prerequisites

Ensure that you have node installed on your machine. The latest node can be download here:

https://nodejs.org/en/download/

### Installing

Clone or fork the repo and open project to root dir.


To run project:

    npm i

    npm start

### Correct Answers

NOTE: The correct answers according to the library will be console logged for references!

## Authors

* **Makeen Sabree** - (https://github.com/msabree)